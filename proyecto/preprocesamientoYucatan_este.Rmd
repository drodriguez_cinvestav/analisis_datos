---
title: "Preprocesamiento de Yucatan Este"
output: html_notebook
---

```{r}
library(readr)
library(fitdistrplus)
library(dummies)
library(dplyr)
library(MASS)

#Funciones
getNA <- function(columna){
  sum(is.na(columna))
}

moda <- function(v){
  uniqv <- unique(v)
  uniqv[which.max(tabulate(match(v,uniqv)))]
} 

outliersReplace <- function(data, lowLimit, highLimit){
  data[data < lowLimit] <- mean(data)
  data[data > highLimit] <- mean(data)
  data       
}
```

```{r}
data = read.csv("data/Diferencial_EMAS-MERRA_Yucatan_Este.csv",header = T, sep=",", na.strings = "NA")
head(data)
```
```{r}
summary(data)
```
```{r}
summary(data$Temp_max_emas)
```


```{r}
str(data)
```

```{r}
print("Missing Data")
cat("Antena: ",getNA(data$Antena),"\n")
cat("Fecha: ",getNA(data$Fecha),"\n")
cat('Latitud: ',getNA(data$Latitud),"\n")
cat('Latitud: ',getNA(data$Longitud),"\n")
cat('Codigo: ',getNA(data$Codigo),"\n")
cat('Temp\\_max\\_emas: ',getNA(data$Temp_max_emas),"\n")
cat('Temp\\_min\\_emas: ',getNA(data$Temp_min_emas),"\n")
cat('Temp\\_max\\_merra: ',getNA(data$Temp_max_merra),"\n")
cat('Temp\\_min\\_merra: ',getNA(data$Temp_min_merra),"\n")
cat('Differential\\_max: ',getNA(data$Differential_max),"\n")
cat('Differential\\_min: ',getNA(data$Differential_min),"\n")
cat("Temp\\_mean\\_merra: ", getNA(data$Temp_mean_merra),"\n")
cat("Temp\\_mean\\_emas: ",getNA(data$Temp_mean_emas),"\n")
cat("Humedad: ",getNA(data$Humedad),"\n")
cat("Presion\\_barometrica: ",getNA(data$Presion_barometrica),"\n")
cat("Precipitacion: ",getNA(data$Precipitacion),"\n")
cat("Radiacion\\_solar: ",getNA(data$Radiacion_solar),"\n")
cat("Etiqueta\\_clase: ",getNA(data$Etiqueta_clase),"\n")
```

```{r}
#Datos faltantes
data_no_na = na.omit(data)
for(i in 1:ncol(data)){
  colum = data[,i]
  c = getNA(colum)
  if (c > 0){
    print(i)
    getNA(data[,i])
    mean_x = mean(data_no_na[,i])
    print(mean_x)
    data[,i][is.na(data[,i])]=mean_x
    getNA(data[,i])
  }
  else{
    print("Sin datos faltantes")
  }
}
#View(data)
```

```{r}
print("Comprobación de datos faltantes")
cat("Antena: ",getNA(data$Antena),"\n")
cat("Fecha: ",getNA(data$Fecha),"\n")
cat('Latitud: ',getNA(data$Latitud),"\n")
cat('Latitud: ',getNA(data$Longitud),"\n")
cat('Codigo: ',getNA(data$Codigo),"\n")
cat('Temp\\_max\\_emas: ',getNA(data$Temp_max_emas),"\n")
cat('Temp\\_min\\_emas: ',getNA(data$Temp_min_emas),"\n")
cat('Temp\\_max\\_merra: ',getNA(data$Temp_max_merra),"\n")
cat('Temp\\_min\\_merra: ',getNA(data$Temp_min_merra),"\n")
cat('Differential\\_max: ',getNA(data$Differential_max),"\n")
cat('Differential\\_min: ',getNA(data$Differential_min),"\n")
cat("Temp\\_mean\\_merra: ", getNA(data$Temp_mean_merra),"\n")
cat("Temp\\_mean\\_emas: ",getNA(data$Temp_mean_emas),"\n")
cat("Humedad: ",getNA(data$Humedad),"\n")
cat("Presion\\_barometrica: ",getNA(data$Presion_barometrica),"\n")
cat("Precipitacion: ",getNA(data$Precipitacion),"\n")
cat("Radiacion\\_solar: ",getNA(data$Radiacion_solar),"\n")
cat("Etiqueta\\_clase: ",getNA(data$Etiqueta_clase),"\n")
```

```{r}
#Normalización de los datos
for(i in 7:length(data)-1){
  data[,i] = (data[,i] - min(data[,i]))/(max(data[,i])-min(data[,i]))
  
}
```



```{r}
jpeg('img/yucatan_este/valores_anomalos/yucatan_este1_clean.jpg')
boxplot(data$Temp_max_emas, data$Temp_min_emas, data$Temp_max_merra, data$Temp_min_merra,
main = "Valores anómalos",
at = c(1,2,4,5),
names = c("Temp_max_emas", "Temp_min_emas", "Temp_max_merra", "Temp_min_merra"),
las = 2,
col = c("orange","red","blue","green"),
border = "black",
horizontal = FALSE,
notch = FALSE
)
dev.off()
```

```{r}
jpeg('img/yucatan_este/valores_anomalos/yucatan_este2_clean.jpg')
boxplot(data$Differential_max, data$Differential_min, data$Temp_mean_merra, data$Temp_mean_emas,
main = "Valores anómalos 2",
at = c(1,2,4,5),
names = c("Differential_max", "Differential_min", "Temp_mean_merra", "Temp_mean_emas"),
las = 2,
col = c("orange","red","blue","green"),
border = "black",
horizontal = FALSE,
notch = FALSE
)
dev.off()
```

```{r}
jpeg('img/yucatan_este/valores_anomalos/yucatan_este3_clean.jpg')
boxplot(data$Humedad, data$Presion_barometrica, data$Precipitacion, data$Radiacion_solar,
main = "Valores anómalos 3",
at = c(1,2,4,5),
names = c("Humedad", "Presion barometrica", "Precipitación", "Radiación solar"),
las = 2,
col = c("orange","red","blue","green"),
border = "black",
horizontal = FALSE,
notch = FALSE
)
dev.off()
```



```{r}
#Outliers
clean_Temp_max_emas <- outliersReplace(data$Temp_max_emas, 0.1, 0.9)
data$Temp_max_emas <- clean_Temp_max_emas
clean_Temp_min_emas <- outliersReplace(data$Temp_min_emas, 0.1, 0.85)
data$Temp_min_emas <- clean_Temp_min_emas
clean_Temp_max_merra <- outliersReplace(data$Temp_max_merra, 0, 0.85)
data$Temp_max_merra <- clean_Temp_max_merra
clean_Temp_min_merra <- outliersReplace(data$Temp_min_merra, 0.1, 1)
data$Temp_min_merra <- clean_Temp_min_merra

clean_Differential_max <- outliersReplace(data$Differential_max, 0.15, 0.65)
data$Differential_max <- clean_Differential_max
clean_Differential_min <- outliersReplace(data$Differential_min, 0.45, 0.93)
data$Differential_min <- clean_Differential_min

#clean_Temp_mean_merra <- outliersReplace(data$Temp_mean_merra, 0, 0.7)
#data$Temp_mean_merra <- clean_Temp_mean_merra

clean_Temp_mean_emas <- outliersReplace(data$Temp_mean_emas, 0.45, 0.6)
data$Temp_mean_emas <- clean_Temp_mean_emas

clean_Humedad <- outliersReplace(data$Humedad, 0.46, 0.55)
data$Humedad <- clean_Humedad
clean_Presion_barometrica <- outliersReplace(data$Presion_barometrica, 0.65, 0.78)
data$Presion_barometrica <- clean_Presion_barometrica
clean_Precipitacion <- outliersReplace(data$Precipitacion, 0, 0.03)
data$Precipitacion <- clean_Precipitacion
clean_Radiacion_solar <- outliersReplace(data$Radiacion_solar, 0.58, 0.72)
data$Radiacion_solar <- clean_Radiacion_solar


boxplot(clean_Temp_max_emas, main='Temp_max_emas')
boxplot(clean_Temp_min_emas, main='Temp_min_emas')
boxplot(clean_Temp_max_merra, main= 'Temp_max_merra')
boxplot(clean_Temp_min_merra, main='Temp_min_merra')

boxplot(clean_Differential_max, main='Differential_max')
boxplot(clean_Differential_min, main='Differential_min')
boxplot(clean_Temp_mean_merra, main='Temp_mean_merra')
boxplot(clean_Temp_mean_emas, main='Temp_mean_emas')

boxplot(clean_Humedad, main='Humedad')
boxplot(clean_Presion_barometrica, main='Presion_barometrica')
boxplot(clean_Precipitacion, main='Precipitacion')
boxplot(clean_Radiacion_solar, main='Radicacion_solar')
```

```{r}

distribuciones= c("norm", "lnorm", "pois", "exp", "gamma", "nbinom", "geom", "beta", "unif", "logis")
dist="unif"

fitg <- fitdist(data$Temp_max_emas, dist)
summary(fitg)
path = paste("img/distribucion_",dist,"_temp_max_emas.jpeg",sep = '')
jpeg(filename=path)
plot(fitg)
dev.off()
```

```{r}
fitg <- fitdist(data$Temp_min_emas, dist)
summary(fitg)
path = paste("img/distribucion_",dist,"_temp_min_emas.jpeg",sep = '')
jpeg(filename=path)
plot(fitg)
dev.off()
```

```{r}
fitg <- fitdist(data$Temp_max_merra, dist,method = "mme")
summary(fitg)
path = paste("img/distribucion_",dist,"_temp_max_merra.jpeg",sep = '')
jpeg(filename=path)
plot(fitg)
dev.off()
```

```{r}
fitg <- fitdist(data$Temp_min_merra, dist,method = "mme")
summary(fitg)
path = paste("img/distribucion_",dist,"_temp_min_merra.jpeg",sep = '')
jpeg(filename=path)
plot(fitg)
dev.off()
```

```{r}
fitg <- fitdist(data$Differential_max, dist,method = "mme")
summary(fitg)
path = paste("img/distribucion_",dist,"_differential_max.jpeg",sep = '')
jpeg(filename=path)
plot(fitg)
dev.off()
```

```{r}
fitg <- fitdist(data$Differential_min, dist)
summary(fitg)
path = paste("img/distribucion_",dist,"_differential_min.jpeg",sep = '')
jpeg(filename=path)
plot(fitg)
dev.off()
```

```{r}
fitg <- fitdist(data$Temp_mean_merra, dist)
summary(fitg)
path = paste("img/distribucion_",dist,"_temp_mean_merra.jpeg",sep = '')
jpeg(filename=path)
plot(fitg)
dev.off()
```

```{r}
fitg <- fitdist(data$Temp_mean_emas, dist)
summary(fitg)
path = paste("img/distribucion_",dist,"_temp_mean_emas.jpeg",sep = '')
jpeg(filename=path)
plot(fitg)
dev.off()
```

```{r}
fitg <- fitdist(data$Humedad, dist)
summary(fitg)
path = paste("img/distribucion_",dist,"_humendad.jpeg",sep = '')
jpeg(filename=path)
plot(fitg)
dev.off()
```

```{r}
which(is.nan(data$Presion_barometrica))
fitg <- fitdist(data$Presion_barometrica, dist,method = "mme")
summary(fitg)
path = paste("img/distribucion_",dist,"_presion_barometrica.jpeg",sep = '')
jpeg(filename=path)
plot(fitg)
dev.off()
```

```{r}
fitg <- fitdist(data$Precipitacion, dist,method = "mme")
summary(fitg)
path = paste("img/distribucion_",dist,"_precipitacion.jpeg",sep = '')
jpeg(filename=path)
plot(fitg)
dev.off()
```

```{r}
fitg <- fitdist(data$Radiacion_solar, dist)
summary(fitg)
path = paste("img/distribucion_",dist,"_radiacion_solar.jpeg",sep = '')
jpeg(filename=path)
plot(fitg)
dev.off()
```

```{r}
columna = data$Radiacion_solar
col = "Yucatan_Este_Radiacion_solar"
fnorm <-fitdist(columna, "norm")
fexp  <-fitdist(columna, "exp") 
fgamma<-fitdist(columna, "gamma", method = "mme")
fbeta <-fitdist(columna, "beta", method = "mme")
funif <-fitdist(columna, "unif")
par(mfrow=c(1,1))
plot.legend<-c("Normal","Exponencial","Gamma","Beta","Uniforme")

path = paste("img/yucatan_este/distribuciones/distribuciones_1",col,".jpeg",sep = '')
jpeg(filename=path)
denscomp(list(fnorm,fexp,fgamma,fbeta,funif), legendtext=plot.legend)
dev.off()

path = paste("img/yucatan_este/distribuciones/distribuciones_2",col,".jpeg",sep = '')
jpeg(filename=path)
qqcomp(list(fnorm,fexp,fgamma,fbeta,funif), legendtext=plot.legend)
dev.off()

path = paste("img/yucatan_este/distribuciones/distribuciones_3",col,".jpeg",sep = '')
jpeg(filename=path)
cdfcomp(list(fnorm,fexp,fgamma,fbeta,funif), legendtext=plot.legend)
dev.off()

path = paste("img/yucatan_este/distribuciones/distribuciones_4",col,".jpeg",sep = '')
jpeg(filename=path)
ppcomp(list(fnorm,fexp,fgamma,fbeta,funif), legendtext=plot.legend)
dev.off()
```

```{r}
#Correlaciones
#View(data[6 : 17])

jpeg("img/yucatan_este/correlaciones/correlacion_pearson.jpeg")
pairs(cor(data[6 : 17], method = "pearson"))
dev.off()
```

```{r}
correlacion_pearson = data.frame(cor(data[6 : 17], method = "pearson"))
correlacion_pearson
write.csv(correlacion_pearson,"img/yucatan_este/correlaciones/cor_pearson.csv", row.names = TRUE)
```

```{r}
#Correlaciones
#View(data[6 : 17])

jpeg("img/yucatan_este/correlaciones/correlacion_spearman.jpeg")
pairs(cor(data[6 : 17], method = "spearman"))
dev.off()
```

```{r}
correlacion_sperman=data.frame(cor(data[6 : 17], method = "spearman"))
correlacion_sperman
write.csv(correlacion_pearson,"img/yucatan_este/correlaciones/cor_spearman.csv", row.names = TRUE)
```

```{r}
#PCA
apply(data[6 : 17], 2, var)
#data.frame(apply(data[6 : 17], 2, var))

pca <- prcomp(data[6 : 17])
pca

```

```{r}
jpeg("img/yucatan_este/pca/pca.jpeg")
plot(pca,type="l")
dev.off()
```

```{r}
summary(pca)
```

```{r}
jpeg("img/yucatan_este/pca/pca_biplot.jpeg")
biplot(pca,scale = 0)
dev.off()
```

```{r}
pc1 <- apply(pca$rotation[,1]*data[6 : 17],1, sum)
pc2 <- apply(pca$rotation[,2]*data[6 : 17],1, sum)
pc3 <- apply(pca$rotation[,3]*data[6 : 17],1, sum)
pc4 <- apply(pca$rotation[,4]*data[6 : 17],1, sum)
pc5 <- apply(pca$rotation[,5]*data[6 : 17],1, sum)

componentes_principales = data.frame(pc1,pc2,pc3,pc4,pc5,data$Etiqueta_clase)
componentes_principales
write.csv(componentes_principales,"img/yucatan_este/pca/pca_yucatan_este5.csv", row.names = FALSE)
```